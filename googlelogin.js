const app = require('./app');
var https = require('https');
var fs = require('fs');


const port =5001;
app.listen(port);

const  options = {
  key: fs.readFileSync("/etc/nginx/ssl/cadenza.tech.key"),
         cert: fs.readFileSync("/etc/nginx/ssl/cadenza.tech.chained.crt")

          }
                   https.createServer(options, function (req, res) {
                                         app.handle(req,res);
                                                    }).listen(5002);
